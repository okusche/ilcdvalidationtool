# ILCD Validation Tool #

This tool can be used to validate datasets in the ILCD data format.

[Developer-README](Developer-README.md)


## Release Notes ##

See the wiki for the [release notes](https://bitbucket.org/okusche/ilcdvalidationtool/wiki/Home).


## Documentation ##

See the [Quick Start Guide](https://bitbucket.org/okusche/ilcdvalidationtool/wiki/Quick%20Start%20Guide)
for an introduction on how to use the tool.


