package com.okworx.ilcd.validation.tool.preferences;

public class PreferenceConstants {
	public static final String PREFSTORE_FILENAME = "showprefs.properties";
	
	public static final String PAGEID_GENERAL = "pref_pageid_general";
	public static final String PAGEID_PROFILES = "pref_pageid_profiles";
	public static final String PAGEID_CATEGORIES = "pref_pageid_categories";
	public static final String PAGEID_REFERENCEFLOWS = "pref_pageid_referenceflows";
	public static final String PAGEID_XMLSCHEMAS = "pref_pageid_xmlschemas";
}
