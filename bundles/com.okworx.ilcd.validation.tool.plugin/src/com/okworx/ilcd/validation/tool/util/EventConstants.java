package com.okworx.ilcd.validation.tool.util;

public class EventConstants {
	public static final String SET_STATUS_TEXT = "SetStatusText";
	public static final String TABLE_ADD_EVENTS = "TableAddEvents";
	public static final String UPDATE_STATISTICS = "UpdateStatistics";
	public static final String RESET_STATISTICS = "ResetStatistics";
	public static final String SET_REPORTSUCCESSES = "SetReportSuccesses";
	public static final String TABLE_CLEAR = "TableClear";
	public static final String STOP_VALIDATION = "StopValidation";
	public static final String UPDATE_PROGRESS = "UpdateProgress";
	public static final String FILE_VALIDATED = "FileValidated";
	public static final String ADD_MENU_ACTIVATION = "AddMenuActivation";
	public static final String REMOVE_MENU_ACTIVATION = "RemoveMenuActivation";
	public static final String CLEAR_MENU_ACTIVATION = "ClearMenuActivation";
	public static final String START_MENU_ACTIVATION = "StartMenuActivation";
	public static final String STOP_MENU_ACTIVATION = "StopMenuActivation";
	public static final String HIDE_OUTDATED_PROFILES = "HideOutdatedProfiles";
}
